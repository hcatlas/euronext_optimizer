# Modern Portfolio Theory (Markowitz-portfolio theory)

* **main idea: a single stock is quite unpredictable BUT we may combine several stocks in order to reduce the risk as much as possible == diversification**

* owing a single stock is risky, but owing several different supports allows one to reduce risk and to attain a **predictable** portfolio;

* **combining stocks reduces their total fluctuation** --> **better signal-to-noise ratio**

## Assumptions of the model:

1.   the returns are normally distributed (related to a mean $\mu$ and a variance $\sigma$)
2.   investors are risk-averse: investors only take more risk if they are expecting more rewards

* low risk --> lower return // high risk --> higher return

* an efficient portfolio has the highest reward for a given level of risk OR the lowest risk for a given reward --> **it optmises the risk-to-gain ratio** (what is done by Yomoni, for instance)

* consider that one can only have long positions and also that the portfolio will be constituted of a set of assets (stocks, for instance) each one having a weight.

## Computation of a daily return of a stock

* Daily return can be calculated on a day-by-day basis, for instance:

$DR =\frac{stock\_price(n)-stock\_price(n-1)}{stock\_price(n-1)}$, where $n$ is the time scale

$DR = log\left(\frac{stock\_price(n)}{stock\_price(n-1)}\right)$

* the $log$ is taken instead of the actual price of stocks in order to impose a normalisation condition (important for ML or Statiscal analyses). It also has something to do with the complexity of the algorithm. The "1" is removed in order to have a positive logarithm

## Computation of the expected return of the portfolio

* it is important that every single asset (stock/security)

* $w_i$ : weight of the $i$-th stock
* $r_i$ : return of the $i$-th stock
* $\mu_i$ : expected return for stock $i$; basically it is the mean.

* the model relies heavily on historical data

* **historical mean performance is assumed to be the best estimator for future (expected) performance**

* **Markowitz theory** says that the **expected return in the future** will be the **same as in the past** --> static model 

* So the expected return of the portfolio $\mu_{portfolio}$ is:

* $\mu_{portfolio} = E(\sum_i w_i r_i) = \sum_i w_i E(r_i) = \sum_i w_i \mu_i = \bar{w}^T\bar{\mu}$ 

* the expected value of the portfolio is the sum of each stock of the weigth times the expected return of the stock --> what is quite logical

* but it turns into computing the mean of the expected return values of each stock

## Computation of the risk of the portfolio

* Risk is correlated to the volatility of the asset, which is correlated to the SD and VAR of it

* Covariance $\sigma$ between two stocks $i$ and $j$ is calculated as:

* $\sigma_{ij} = E[(r_i-\mu_i)(r_j-\mu_j)]$

* Covariance measures how much two random variables vary together:

* If $\sigma_{ij} < 0$ the assets' variations are not correlated (returns move invertly)

* If $\sigma_{ij} > 0$ the assets move together

* **Markowitz theory**: diversification in order to reduce risk BUT these stocks must be **uncorrelated** (negative covariance) 

* The variance VAR/$\sigma_i$ is the covariance of a variable with itself, so:

* $\sigma_i^2 = E[(r_i-\mu_i)^2]$

* Given that risk is linked to volatility which is linked to variance, **we should have the covariance matrix of all the stocks of a portfolio** == **$\Sigma$ matrix**

* variance is the relationship of one stock and covariance is between two stocks

* the diagonal items are the variance and off-diagonal are the covariance

* the matrix elements are obtained through historical data

* the covariance matrix contains the relationship between all the stocks of the portfolio 

* so we are able to construct the **expected variance (squared) of the portfolio** $\sigma_p^2$ as:

* $\sigma_p^2 = E[(r_i-\mu_i)^2] = \sum_i \sum_j w_i w_j \sigma_{ij} = \bar{w}^T\bar{\Sigma}\bar{w}$

* **Monte-Carlo can generate different random portfolios (weight vectors) for which the volatility and return can be evaluated** ==> this gives rise to an **efficient-frontier** plot

* the investor wants:

1.   the maximum return given a fixed risk level
2.   the minimum risk given a fixed return

* the investor can decide the risk and the expected return based on a Markowitz model plot

## Sharpe-ratio

* it is the most import risk/return measure in Finance

* it describes how much excess return you receive for extra volatility that you endure holding a riskier asset

* $S(x) = \frac{r_x-R_f}{SD(x)}$, where $r_x$ is the average rate of return of investment $x$ and $R_f$ is the rate of return of risk-free security

* the risk-free rate $R_f$ corresponds to a very low risk investment such as a government bond (around 1%)

* the SD corresponds to the volatility of the investment; **recall that $SD = \sqrt{\sigma^2}$**

* A Sharpe-ration $S(x) > 1$ is considered to be good

* if volatility is high (asset's value varies a lot), the Sharpe-ratio is low

* one must choose the highest Shape-ratio portfolio (within the Markowitz-portfolio theory)

## Capital allocation line

* line tangent to the efficient-frontier at the maximum Sharpe-ratio point;

* if risk -> 0, one can find a portfolio with a very low risk and with some return (government bonds and treasury bills)

* **if the portfolio can contain stocks and risk-free assets, than it the best possible portfolio lies on the capital allocation line**