import os
import numpy as np
import pandas as pd
import pandas_datareader.data as web
import datetime
from .stocks import stock_dict

def dates(start=5, unities='years', end='today'):    
    today = datetime.date.today().strftime("%Y-%m-%d")
    if unities == 'years':
        delta = (datetime.datetime.now() - datetime.timedelta(days=start*365)).strftime("%Y-%m-%d")
    if unities == 'months':
        delta = (datetime.datetime.now() - datetime.timedelta(days=start*30)).strftime("%Y-%m-%d")
    if unities == 'days':
        delta = (datetime.datetime.now() - datetime.timedelta(days=start)).strftime("%Y-%m-%d")    
    return delta, today

def download_data(stocks, start_date, end_date):
    data = web.DataReader(list(stocks.keys()), data_source='yahoo', 
                        start=start_date, end=end_date)['Adj Close']
    data = data.fillna(method='pad')
    return data

def GetData(today, tolerance_days=3):
    try:
        df = pd.read_csv(os.path.join(os.getcwd(), 'data', 'cac40.csv.gz'), compression='gzip', index_col=0)
    except FileNotFoundError:
        start_date = (datetime.datetime.now() - datetime.timedelta(days=10*365))######
        download_data(stock_dict, start_date.strftime("%Y-%m-%d"), today.strftime("%Y-%m-%d")).to_csv(os.path.join(os.getcwd(), 'data', 'cac40.csv.gz'), compression='gzip')
        df = pd.read_csv(os.path.join(os.getcwd(), 'data', 'cac40.csv.gz'), compression='gzip', index_col=0)
    last_day = datetime.datetime.strptime(df.iloc[-1:,:0].index[0], "%Y-%m-%d")  # %H:%M:%S
    diff = today - last_day
    diff = divmod(diff.days, 60)[1]
    if diff > tolerance_days:
        start_date, end_date = dates(start=diff-1, unities='days', end='today')
        #data = download_data(stock_dict, start_date.strftime("%Y-%m-%d"), end_date.strftime("%Y-%m-%d"))
        data = download_data(stock_dict, start_date, end_date)
        dfc = pd.concat([df, data], axis=0)
        dfc.to_csv(os.path.join(os.getcwd(), 'data', 'cac40.csv.gz'), compression='gzip')
        return dfc
    else:
        return pd.read_csv(os.path.join(os.getcwd(), 'data', 'cac40.csv.gz'), compression='gzip', index_col=0)
    
def DataSlicer(data, old, new):
    """
    If a problem arrives on the definition of idx_old or idx_new, it comes from the range that must be increased
    """
    for delta in range(0,45): 
        try:
            idx_old = data.index.get_loc((old - datetime.timedelta(days=delta)).strftime("%Y-%m-%d"))
        except KeyError:
            continue
        break

    for delta in range(0,45):
        try:
            idx_new = data.index.get_loc((new - datetime.timedelta(days=delta)).strftime("%Y-%m-%d"))
        except KeyError:
            continue
        break
    return data.iloc[idx_old:idx_new+1,:]
