#https://github.com/PrateekKumarSingh/Python/tree/master/Python%20for%20Finance/Python-for-Finance-Repo-master

import numba as nb
import numpy as np
import pandas as pd
from scipy import optimize as opt

def calculate_returns(data):
    """value of stock at day n - value of stock at day n-1 
    (that's why the shift(1)))
    it measures how much it changes from one day to the other"""
    returns = np.log(data/data.shift(1)) 
    return returns

def initialize_weights(data):
    number = len(data.columns)
    weights = np.random.random(number)
    weights /= np.sum(weights)
    return weights

def generate_better_weights(data, amount_available, last_price, quantity=500000):
    """
    This function initializes a lot of possible weights and try a first optimization of the array by taking the one
    which yields the lowest delta between the total portfolio value obtained with it and the total available amount of
    cash for the investment. 
    """
    ws = []
    deltas = []
    
    number = len(data.columns)
    for i in range(quantity):
        weights = np.random.random(number)
        weights /= np.sum(weights)
        delta = amount_available-(np.sum(np.array(np.round((weights*amount_available)/last_price), dtype=int)*last_price))
        ws.append(weights)
        deltas.append(delta)

    weights = np.array(ws)
    deltas = np.array(deltas)
    return weights, deltas

def statistics(weights, returns):
    portfolio_return = np.sum(returns.mean()*weights)*252
    portfolio_volatility = np.sqrt(np.dot(weights.T, np.dot(returns.cov()*252, weights)))
    return np.array([portfolio_return, portfolio_volatility, portfolio_return/portfolio_volatility])

def min_func_sharpe(weights, returns, by='sharpe'):
    """[2] means that we want to maximize according to Sharpe ratio
    note: maximize f(x) is the same of minimizing -f(x)"""
    if by == 'returns':
        return -statistics(weights, returns)[0]
    elif by == 'volatility':
        return statistics(weights, returns)[1]
    elif by == 'sharpe':
        return -statistics(weights, returns)[2]
    else:
        raise ValueError('Optimization criterium has not been provided.')

def optimize_portfolio(weights, returns, by, bounds, constraints, method):
    """the constraints are the sum of the weigths that should equal 1
    so, f(x)=0 is the function to minimize"""
    optimum = opt.minimize(fun=min_func_sharpe, 
                           x0=weights,
                           args=(returns, by), 
                           method=method, #'SLSQP' or 'trust-constr'
                           bounds=bounds, 
                           constraints=constraints
                          )        
    
    return optimum['success'], optimum['x']

@nb.jit(nopython=True, parallel=True)
def get_preturns(returns_mean, weights_array):
    preturns = []
    for w in weights_array:
        portfolio_return = np.sum(returns_mean*w)*252
        preturns.append(portfolio_return)
    preturns = np.array(preturns)
    return preturns

@nb.jit(nopython=True, parallel=True)
def get_pvariances(returns_covariance, weights_array):
    pvariances = []
    for w in weights_array:
        portfolio_volatility = np.sqrt(np.dot(w.T, np.dot(returns_covariance*252, w)))
        pvariances.append(portfolio_volatility)
    pvariances = np.array(pvariances)
    return pvariances

@nb.jit(nopython=True, parallel=True)
def get_psharpes(preturns, pvariances):
    return np.divide(preturns, pvariances)

def portfolio_statistics(opt_weights, returns):
    portfolio_return = np.sum(returns.mean()*opt_weights)*252
    portfolio_volatility = np.sqrt(np.dot(opt_weights.T, np.dot(returns.cov()*252, opt_weights)))
    return {'portfolio_return': np.round(portfolio_return, 2), 
            'portfolio_volatility': np.round(portfolio_volatility, 2), 
            'portfolio_sharpe': np.round(portfolio_return/portfolio_volatility, 2)}

#def show_statistics(returns):
#    """there are 252 trading days in a year"""
#    print(returns.mean()*252)
#    print(returns.cov()*252)
#    return 

#def calculate_portfolio_return(returns, weights):
#    portfolio_return = np.sum(returns.mean()*weights)*252
#    print("Expected portfolio return:", portfolio_return)
#    return 

#def calculate_portfolio_variance(returns, weights):
#    """attention to the fact that this is not truly the variance but the SD"""
#    portfolio_variance =  np.sqrt(np.dot(weights.T, np.dot(returns.cov()*252, weights)))
#    print("Expected variance:", portfolio_variance)
#    return 

#def statistics(weights, returns):
#    portfolio_return = np.sum(returns.mean()*weights)*252
#    portfolio_volatility = np.sqrt(np.dot(weights.T, np.dot(returns.cov()*252, weights)))
#    return np.array([portfolio_return, portfolio_volatility, portfolio_return/portfolio_volatility])

#def min_func_sharpe(weights, returns):
#    """[2] means that we want to maximize according to Sharpe ratio
#    note: maximize f(x) is the same of minimizing -f(x)"""
#    return -statistics(weights, returns)[0]

#def optimize_portfolio(weights, returns, data, max_price):
#    """the constraints are the sum of the weigths that should equal 1
#    so, f(x)=0 is the function to minimize. 
#    also, the optimum solution should respect the distribution of initial capital max_price 
#    considering the price of the stock of the last day"""
#    idx_fdj = np.where(data.columns == 'FDJ.PA')[0][0]
#    idx_stm = np.where(data.columns == 'STM.PA')[0][0]
#    idx_veo = np.where(data.columns == 'VIE.PA')[0][0]
#    last_price = np.array(data.iloc[-1,:])
#    constraints = ({'type': 'eq', 'fun': lambda x: np.sum(x)-1}, 
#                   {'type':'ineq', 'fun': lambda x: x},
#                   {'type': 'ineq', 'fun': lambda x: max_price-np.sum(x*last_price)},
#                   {'type': 'ineq', 'fun': lambda x: (x[idx_fdj]*max_price)-(117*last_price[idx_fdj])},
#                   {'type': 'ineq', 'fun': lambda x: (x[idx_stm]*max_price)-(10*last_price[idx_stm])},
#                   {'type': 'ineq', 'fun': lambda x: (x[idx_veo]*max_price)-(8*last_price[idx_veo])},
#                  ) 
#    
#    bounds = tuple((0,1) for x in range(len(data.columns)))
#    optimum = sco.minimize(fun=min_func_sharpe, x0=weights, args=returns, method='SLSQP',
#                           bounds=bounds, constraints=constraints)
#    return optimum

#def print_optimal_portfolio(optimum, returns):
#    print("Optimal weights:", optimum['x'].round(3))
#    print("Expected return, volatility and Sharpe ratio:", statistics(optimum['x'].round(3), returns))
#    return

#def present_portfolio(optimum, data, stocks, max_price):
#    last_price = np.array(data.iloc[-1,:])
#    portfolio = optimum['x']
#    nbr_stocks = np.array(np.round((portfolio*max_price)/last_price), dtype=int)
#    stocks_names = np.array(data.iloc[-1,:].index)
#    total_value = np.round(np.sum(nbr_stocks*last_price), 2)
#    for i, j, k in zip(nbr_stocks, last_price, stocks_names):
#        if i > 0:
#            print('{} actions de {} à {} chacune faisant un total de {}'.format(i, stocks[k], round(j,2), round(i*j, 2)))
#    print ('Le coût total du portefeuille s\'élève à {}'.format(total_value))
#    return
