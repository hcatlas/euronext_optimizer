import numpy as np
import pandas as pd
import pandas_datareader.data as web
import matplotlib.pyplot as plt
import datetime
from datetime import date
import holoviews as hv 
from holoviews import opts

def plot_portfolios(dataframe, optimized_portfolio):
    """c is the Sharpe ratio w/o risk-free asset
    one can use the either the variance or the standard deviation"""
    data = {
        'x': dataframe['pvariances'],
        'y': dataframe['preturns'],
        'z': dataframe['psharpes']}
    plot_opts = {'height': 400, 
                 'width': 500, 
                 'colorbar': True, 
                 'xlabel': 'Expected Volatility',
                 'ylabel': 'Expected Return',
                 'zlabel': 'Sharpe ratio',
                 #'xlim': (0.9*dataframe['pvariances'].min(), 1.1*dataframe['pvariances'].max()),
                 #'ylim': (0.9*dataframe['preturns'].min(), 1.1*dataframe['preturns'].max()),
                 }
    style_opts = {'cmap': 'viridis', 'color':'z', 'size': 8}
    fig1 = hv.Scatter(data, vdims=['y', 'z']).opts(plot=plot_opts, style=style_opts)
    
    portfolio = {
    'x': optimized_portfolio['portfolio_volatility'],
    'y': optimized_portfolio['portfolio_return'],
    'z': optimized_portfolio['portfolio_sharpe']}
    style_opts2 = {'color':'k', 'size': 12, 'marker': 's'}
    fig2 = hv.Scatter(portfolio).opts(style=style_opts2)
    
    return fig2 * fig1

def plot_portfolio_evolution(portfolio_data_cut):
    fig = portfolio_data_cut.hvplot(xticks=15,
           ylabel='Variation du prix en €',
           width=800, height=400)
    return fig